# NTA LuxWater paper data
## by Dagny Aurich

This is a repository including all the data, code and figures related to the paper: "Non-Target Screening of Surface Water Samples to Identify Exposome-Related Pollutants: A Case Study from Luxembourg" written by D.Aurich, P. Diderich, R. Helmus, and E. L. Schymanski

- output summary files from patRoon
- R code for patRoon workflow and feature optimization (IPO)
- figures
- full ClassyFire Results
- supplementary tables and figures
- additional data, including R versions and sampling locations per sample id 

# License

Content in this repository is shared under these license conditions:

-	Code: Artistic-2.0
-	Data: CC-BY 4.0 [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/legalcode) 

Copyright (c) 2017-2023 University of Luxembourg


