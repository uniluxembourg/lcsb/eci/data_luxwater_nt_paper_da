# Non target LuxWater patRoon paper, Sets workflow, Jan 2023, modified by Dagny Aurich

# required packages:
library(patRoon)
library(xcms)
library(BiocParallel)
options(patRoon.path.MetFragPubChemLite = "data/PubChemLite_exposomics_20230127.csv")

# -------------------------
# initialization
# -------------------------

month <- 'Aug'
year <- 2019
workPath <- "data/year/month"
workPath <- sub('month', month , workPath)
workPath <- sub('year', year , workPath)
setwd(workPath)

# Load analysis table
anaInfoPos <- read.csv("analyses-pos.csv")
anaInfoNeg <- read.csv("analyses-neg.csv")

# Set the name of the output folder
output <- "report_monthyear"
output <- sub('month', month , output)
output <- sub('year', year , output)

# -------------------------
# features
# -------------------------
# Feature parameter optimization, see feature_optimization_paper.R
#optimization workflow not applicable for feature grouping parameters in Sets workflow

# -------------------------
# Find all features
# insert optimized ppm and peakwidth or leave defaults

fListPos <-
  findFeatures(
    anaInfoPos,
    "xcms3",
    param = xcms::CentWaveParam(
      ppm = 5,
      peakwidth = c(12, 60),
      snthresh = 10,
      prefilter = c(3, 100),
      mzCenterFun = "wMean",
      integrate = 1,
      mzdiff = -0.001,
      fitgauss = FALSE,
      noise = 2500,
      verboseColumns = FALSE,
      roiList = list(),
      firstBaselineCheck = TRUE,
      roiScales = numeric(0),
      extendLengthMSW = FALSE
    )
  )
fListNeg <-
  findFeatures(
    anaInfoNeg,
    "xcms3",
    param = xcms::CentWaveParam(
      ppm = 5,
      peakwidth = c(12, 60),
      snthresh = 10,
      prefilter = c(3, 100),
      mzCenterFun = "wMean",
      integrate = 1,
      mzdiff = -0.001,
      fitgauss = FALSE,
      noise = 2500,
      verboseColumns = FALSE,
      roiList = list(),
      firstBaselineCheck = TRUE,
      roiScales = numeric(0),
      extendLengthMSW = FALSE
    )
  )

fList <-
  makeSet(fListPos, fListNeg, adducts = c("[M+H]+", "[M-H]-"))

# Group and align features between analyses
# to disable parallelization when required:
BiocParallel::register(BiocParallel::SerialParam(), default = TRUE)

fGroups <- groupFeatures(fList, "xcms3")

# Basic rule based filtering of fGroups
fGroups <-
  filter(
    fGroups,
    absMinIntensity = 100,
    relMinReplicateAbundance = NULL,
    maxReplicateIntRSD = NULL,
    blankThreshold = 3,
    removeBlanks = TRUE,
    retentionRange = NULL,
    mzRange = NULL
  )

# -------------------------
# annotation
# -------------------------

# Retrieve MS peak lists
avgMSListParams <- getDefAvgPListParams(clusterMzWindow = 0.005)
mslists <-
  generateMSPeakLists(
    fGroups,
    "mzr",
    maxMSRtWindow = 5,
    precursorMzWindow = 0.5,
    avgFeatParams = avgMSListParams,
    avgFGroupParams = avgMSListParams
  )

# Rule based filtering of MS peak lists
mslists <-
  filter(
    mslists,
    withMSMS = TRUE,
    absMSIntThr = NULL,
    absMSMSIntThr = NULL,
    relMSIntThr = NULL,
    relMSMSIntThr = NULL,
    topMSPeaks = NULL,
    topMSMSPeaks = 25,
    deIsotopeMS = FALSE,
    deIsotopeMSMS = FALSE
  )

# Calculate compound structure candidates
# scoreTypes to be defined, e.g.: "fragScore","metFusionScore","score","individualMoNAScore","AnnoTypeCount","Patent_Count", "DisorderDisease", "PubMed_Count"
compounds <-
  generateCompounds(
    fGroups,
    mslists,
    "metfrag",
    method = "CL",
    dbRelMzDev = 5 ,
    fragRelMzDev = 5,
    fragAbsMzDev = 0.002,
    database = "pubchemlite",
    setThresholdAnn = 0,
    scoreTypes = c("individualMoNAScore", "fragScore", "score", "PubMed_Count"),
    maxCandidatesToStop = 100,
    timeoutRetries = 20
  )
# -------------------------
# reporting
# -------------------------
#Create the csv tables with groups and peak intensities for the set (pos and neg results combined)
reportCSV(
  fGroups,
  path = output,
  reportFeatures = FALSE,
  compounds = compounds,
  compoundsNormalizeScores = "max"
)
# individual feature groups tables for pos and neg mode
# get a table with all feature group data for positive mode
pos_features <- as.data.table(unset(fGroups, "positive"))
write.csv(pos_features, "pos_features.csv")
# same for negative, but also add individual feature data
neg_features <-
  as.data.table(unset(fGroups, "negative"))
write.csv(neg_features, "neg_features_test.csv")

# Summary of MetFrag Results in a a Single Table
MFsummary <- as.data.table(compounds)
outputSummary <- paste(output, "MFsummary.csv", sep = "/")
write.csv(MFsummary, outputSummary)

#Plots of every group of features (in the set)
reportPDF(
  fGroups,
  path = output,
  reportFGroups = TRUE,
  reportFormulaSpectra = TRUE,
  compounds = compounds,
  compoundsNormalizeScores = "max",
  MSPeakLists = mslists
)

