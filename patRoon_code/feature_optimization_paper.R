# NT LuxWater patRoon paper, Jan 2023, DA
# feature parameter optimization steps (based on IPO): 

# -------------------------
# initialization
# -------------------------
library(patRoon)

month <- 'Sep'
year <- 2019
workPath <- "data/year/month"
workPath <- sub('month', month , workPath)
workPath <- sub('year', year , workPath)
setwd(workPath)

# Load analysis table
anaInfoPos <- read.csv("analyses-pos.csv")
anaInfoNeg <- read.csv("analyses-neg.csv")

# define parameter sets
pSet <-
  list(
    method = "centWave",
    ppm = c(5, 10),
    min_peakwidth = c(5, 20),
    max_peakwidth = c(30, 60)
  )
# otimize feature finding parameters
ftOptPos <- optimizeFeatureFinding(anaInfoPos, "xcms3", pSet)
ftOptNeg <- optimizeFeatureFinding(anaInfoNeg, "xcms3", pSet)

#best settings:
ParamPOS <- optimizedParameters(ftOptPos)
ParamNeg <- optimizedParameters(ftOptNeg)

#optimization workflow not applicable for feature grouping parameters in Sets workflow
